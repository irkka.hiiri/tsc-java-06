# Task Manager 

Training project.  
A simple console application for managing task lists.

## TECH STACK

Java Core

## SOFTWARE

* JDK 1.8
* Maven 3.6.3
* Windows 10 64-bit/Linux Ubuntu 18.04 x86_64

## HARDWARE

* Core i7-4790 or Ryzen 3 3200G.
* GTX 1060 6GB, GTX 1660 Super (or R9 Fury)
* 12GB RAM.
* 6GB VRAM.
* 70GB SSD storage.

## BUILD 

```
mvn clean install
```

## RUN   

```
java -jar ./target/task-manager.jar
```

## DEVELOPER 

Irina Chaplygina  
Technoserv Consulting  
ichaplygina@tsconsulting.com

## SCREENSHOTS

![run with parameters](https://gitlab.com/irkka.hiiri/tsc-java-misc/-/raw/master/screenshots/tsc-java-06/1_with_params.png)
  
![run with no args](https://gitlab.com/irkka.hiiri/tsc-java-misc/-/raw/master/screenshots/tsc-java-06/2_no_params.png)
