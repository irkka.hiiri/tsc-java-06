package ru.tsc.ichaplygina.taskmanager;

import ru.tsc.ichaplygina.taskmanager.constant.TerminalConst;

import java.util.Scanner;

public class TaskManager {

    public static void main(final String[] args) {
        run(args);
    }

    public static void run(final String[] params) {
        if (params == null || params.length == 0) processInput();
        else executeCommand(params[0]);
    }

    public static void processInput() {
        showWelcome();
        final Scanner scanner = new Scanner(System.in);
        String command = readCommand(scanner);
        while (!TerminalConst.CMD_EXIT.equals(command)) {
            executeCommand(command);
            command = readCommand(scanner);
        }
    }

    public static void executeCommand(final String command) {
        if (command.isEmpty()) return;
        switch (command) {
            case TerminalConst.CMD_ABOUT:
                showAbout();
                break;
            case TerminalConst.CMD_VERSION:
                showVersion();
                break;
            case TerminalConst.CMD_HELP:
                showHelp();
                break;
            default:
                showUnknown(command);
        }
    }

    public static String readCommand(final Scanner scanner) {
        showCommandPrompt();
        return scanner.nextLine().trim();
    }

    public static void showWelcome() {
        System.out.println("\n*** WELCOME TO THE ULTIMATE TASK MANAGER ***\n");
        System.out.println("Enter help to show available commands.");
        System.out.println("Enter exit to quit.");
        System.out.println("Enter command:\n");
    }

    public static void showCommandPrompt() {
        System.out.print("> ");
    }

    public static void showVersion() {
        System.out.println();
        System.out.println("Version: 0.2.0");
        System.out.println();
    }

    public static void showAbout() {
        System.out.println();
        System.out.println("Developed by:");
        System.out.println("Irina Chaplygina,");
        System.out.println("Technoserv Consulting,");
        System.out.println("ichaplygina@tsconsulting.com");
        System.out.println();
    }

    public static void showHelp() {
        System.out.println();
        System.out.println("Available commands:");
        System.out.println(TerminalConst.CMD_VERSION + " - show version info;");
        System.out.println(TerminalConst.CMD_ABOUT + " - show developer info;");
        System.out.println(TerminalConst.CMD_HELP + " - show this message.");
        System.out.println();
    }

    public static void showUnknown(final String arg) {
        System.out.println();
        System.out.println("Unknown argument " + arg);
        System.out.println();
    }

}
